'use strict'

const Product = require('../models/product')

function getProduct (req, res) {
	let productId = req.params.productId

	Product.findById(productId, (err, product) => {
		if (err) return res.status(500).send({message: `Error al realizar la petición: ${err}`})
		if (!product) return res.status(404).send({message: `El producto no existe`})

		res.status(200).send({ product })
	})
}

function getProducts (req, res) {
	Product.find({}, (err, products) => {
		if (err) return res.status(500).send({message: `Error al realizar la petición: ${err}`})
		if (!products) return res.status(404).send({message: 'No existen productos'})

		res.status(200).send({ products })
	})
}

function saveProduct (req, res) {
	let product = new Product({
		_id : req.body._id,
		name : req.body.name,
		price : req.body.price,
		category : req.body.category,
		description : req.body.description,
	});

	product.save((err, productStored) => {
		if (err) res.status(500).send({message:`Error al salvar en la Base de Dato: ${err}`})
		res.status(200).send({product:productStored})
	});
};

function updateProduct (req, res) {
	let productId = req.params.productId;
	let update = req.body;
	delete update._id;

	Product.findByIdAndUpdate(productId, update, (err, productUpdated) => {
		if (err) res.status(500).send({message: `Error al actualizar el producto: ${err}`})

		res.status(200).send({ message: 'El producto ha sido actualizado' })
	})
}

function deleteProduct (req, res) {
	let productId = req.params.productId

	Product.findById(productId, (err, product) => {
		if (err) res.status(500).send({message: `Error al borrar el producto: ${err}`})

		product.remove((err, products) => {
			if (err) res.status(500).send({message: `Error al borrar el producto: ${err}`})
			res.status(200).send({products})
		})
	})
}

module.exports = {
	getProduct,
	getProducts,
	saveProduct,
	updateProduct,
	deleteProduct
}