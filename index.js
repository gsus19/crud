'use strict'

const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();
const port = process.env.PORT || 3001;

const Product = require('./models/product');
const api = require('./routes')


app.use(cors());
app.use(bodyParser.urlencoded({ extended : false }));
app.use(bodyParser.json());

app.use('/api', api)

mongoose.connect('mongodb://gsusrb:45215061@ds255889.mlab.com:55889/products', (err, res) => {
	if(err) throw err
	console.log('Conexion a la base de datos establecida');

	app.listen(port, () => {
		console.log(`API REST corriendo en http://localhost:${port}`)
	});

})


