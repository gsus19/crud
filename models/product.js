'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProductSchema = Schema({
	_id : {type: Number, required : true},
	name: {type:String, required : true},
	price: { type: Number, default: 0 },
	category: { type: String, enum: ['computers', 'phones', 'tvs'] },
	description: String
})

module.exports = mongoose.model('Product', ProductSchema)

